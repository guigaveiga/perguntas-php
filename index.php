<?php session_start() ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <title>Mesa Redonda</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Damion&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
 <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <div class="container mx-auto my-3">
    <h2 class="title">O que você deseja saber?</h2>
    <div class="inside">
      <form method="post">
        <h3>1.Para quem deseja perguntar?</h3>
        <small>Clique sobre a imagem para selecionar a quem se direciona sua pergunta.</small>
        <div class="cc-selector">
          <input id="aline" type="radio" name="palestrante" value="aline" />
          <label class="drinkcard-cc aline" for="aline"></label>
          <input id="ivia" type="radio" name="palestrante" value="ivia" />
          <label class="drinkcard-cc ivia" for="ivia"></label>
          <input id="mara" type="radio" name="palestrante" value="mara" />
          <label class="drinkcard-cc mara" for="mara"></label>
          <input id="micheline" type="radio" name="palestrante" value="micheline" />
          <label class="drinkcard-cc micheline" for="micheline"></label>
          <input id="rosa" type="radio" name="palestrante" value="rosa" />
          <label class="drinkcard-cc rosa" for="rosa"></label>
        </div>
        <h2 id="ask">2. Digite sua pergunta</h2>
        <textarea name="pergunta"></textarea>
        <div class="text-center mt-2">
          <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>
    </div>

  </div>
  
  <div class="modal modal-sm" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content pop">
      <div class="modal-header">
        <h4 class="alert-heading title ml-5">Sua pergunta foi enviada!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ml-5">
        <p>Ela poderá ser selecionada e encaminhada a uma palestrante. Você pode fazer mais perguntas, se desejar!</p>
        <p>Agradecemos sua participação</p>
      </div>
    </div>
  </div>
</div>
</body>
  <?php
  
  if( $_SERVER['REQUEST_METHOD']=='POST' )
	{
		$request = md5( implode( $_POST ) );
		
		if( isset( $_SESSION['last_request'] ) && $_SESSION['last_request']== $request )
		{
			//fazer nada
		}
		else
		{
			$_SESSION['last_request']  = $request;
			 if(!empty($_POST)) {
    $arq = "bd/perguntas.json";
    $data = file_get_contents("$arq"); 
    $json = json_decode($data,TRUE);      
    array_push($json,$_POST);
    $json_enc = json_encode($json);    
    file_put_contents("$arq", $json_enc);
  }
		}
	}
  
 
  
  ?>
  <script>
  $("input[name=palestrante]").on("click", function(e){
    targetOffset = $("#ask").offset().top;
    $('html, body').animate({
        scrollTop: targetOffset - 90
    }, 700);
  });
  
    $("form").one("submit", function(e){ 
      e.preventDefault();
       $('.modal').show();
        
      setTimeout(function() {
        $('form').submit();
        }, 3500);
        
      //location.reload();
    })
  
    
  </script>