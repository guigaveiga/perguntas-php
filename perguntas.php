<?php
$arquivo = file_get_contents("bd/perguntas.json");
$json = json_decode($arquivo);
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <title>Mesa Redonda</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Damion&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script></head>
 
  <body>
    <div class="container mx-auto my-3">
      <div class="inside mt-2">
        <table id="perguntas" class="display col-10" >
        <thead>
          <th>#</th>
          <th>Palestrante</th>
          <th>Pergunta</th>
      </thead>
      <tbody>
      <?php
        foreach($json as $k=>$v){
          echo "<tr><td></td>";
          echo "<td>".@$v->palestrante."</td>";
          echo "<td>".@$v->pergunta."</td>";
          echo "</tr>";
        }
      ?>
      </tbody>
    </table>
      </div>
      
    </div>
    
   <script>
     
  $(document).ready(function() {
    var t;
    t = $('#perguntas').DataTable( {
      "language": {
          "lengthMenu": "Mostrar _MENU_ por página",
          "zeroRecords": "Nenhum Registro Encontrado",
          "info": "Mostrando página <strong> _PAGE_ </strong> de <strong> _PAGES_ </strong>",
          "infoFiltered": "(Total de dados filtrados _MAX_)",
          "sSearch": "Pesquisar:",
          paginate: {
            first:      "Primeira",
            previous:   "Anterior",
            next:       "Próxima",
            last:       "Última"
        },
        },
        "pageLength": 100,
        "stateSave": true,
        "responsive": true,
        "order": [
          [1, "asc"]
        ],
        "lengthMenu": [
          [10, 25, 50, 100, -1],
          [10, 25, 50, 100, "TODOS"]
        ]
      });
      t.on('order.dt search.dt', function() {
        t.column(0, {
          search: 'applied',
          order: 'applied'
        }).nodes().each(function(cell, i) {
          cell.innerHTML = i + 1;
        });
      }).draw();
    
    } );
  </script>
  </body>